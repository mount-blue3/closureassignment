function limitFunctionCallCount(cb,n){
    if(typeof cb !== 'function' || n<0||typeof n !== 'number'){
        throw new Error("check if it is a function or not and check n value");
    }
    let count = 0;
    return function(...a){
        if(count<n){
            count++;
            return cb(...a);
        }
        else{
            return null;
        }
    }
}
module.exports = limitFunctionCallCount;