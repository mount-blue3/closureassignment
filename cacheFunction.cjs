function cacheFunction(cb){
    if(typeof cb !== 'function'){
        throw new Error("check if it is a function or not");
    }
    const cache = {};
    return function(...value){
        if(!cache[value]){
            let result=cb(...value);
            cache[value]=result;
        }
        return cache[value]
    }
}
module.exports = cacheFunction;