function counterFactory(val=0){
    return{
        increment: function(){
            return ++val;
        },
        decrement: function(){
            return --val;
        }
    };
}
module.exports = counterFactory;