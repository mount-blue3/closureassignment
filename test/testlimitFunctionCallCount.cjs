const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");
let n = 4;
function cb(x) {
  x *= 10;
  return x;
}
try {
  const res = limitFunctionCallCount(cb, n);
  console.log(res(5));
  console.log(res(5));
  console.log(res(5));
  console.log(res(5));
  console.log(res(5));
  console.log(res(5));
} catch (e) {
  console.error(e.message);
}
//let res = limitFunctionCallCount(cb,n);
