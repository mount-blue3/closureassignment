const cacheFunction = require("../cacheFunction.cjs");
function cb(x) {
  console.log("This is the first time");
  return ++x;
}
//cb = 10;
try {
  let res = cacheFunction(cb);
  console.log(res(4));
  console.log(res(5));
  console.log(res(6));
  console.log(res(4));
} catch (e) {
  console.error(e.message);
}
